import os

ABSOLUTE_PATH = os.path.dirname(__file__)
INPUT_PATH = os.path.join(ABSOLUTE_PATH, "input.txt")


def sum_rucksack_items_part_1(sacks: list):
    p_str = '-abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
    sum = 0
    for s in sacks:
        half = len(s) // 2
        first = s[half:]
        second = s[:half]
        for char in first:
            if char in second:
                p = p_str.index(char)
                sum += p
                break
    
    return sum


def sum_rucksack_items_part_2(sacks: list, group_num: int):
    p_str = '-abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
    sum = 0

    chunks = chunk_list(sacks, group_num)

    for c in chunks:

        first = c[0]
        second = c[1]
        third = c[2]
        for char in first:
            if char in second and char in third:
                p = p_str.index(char)
                sum += p
                break
    
    return sum

def chunk_list(l: list, n):
    for i in range(0, len(l), n):
        yield l[i:i + n]


    
def main():

    with open(INPUT_PATH, 'r') as f:
        sacks = f.readlines()
        print(sum_rucksack_items_part_1([r.strip().replace(" ", "") for r in sacks]))
        print(sum_rucksack_items_part_2([r.strip().replace(" ", "") for r in sacks], 3))
    
if __name__ == "__main__":
    main()