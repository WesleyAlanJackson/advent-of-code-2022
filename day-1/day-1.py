import os

ABSOLUTE_PATH = os.path.dirname(__file__)
INPUT_PATH = os.path.join(ABSOLUTE_PATH, "input.txt")

def sum_top_n_packs_with_most_calories(packs: list, num_packs: int):
    pack_totals = []
    s = 0
    for p in packs:
        if p.strip():
            s += int(p.strip())
        else:
            pack_totals.append(s)
            s = 0
    
    return sum(sorted(pack_totals, key=int, reverse=True)[:num_packs])

    
def main():

    with open(INPUT_PATH, 'r') as f:
        packs = f.readlines()
        calories = sum_top_n_packs_with_most_calories(packs, 3)
        print(calories)
    
if __name__ == "__main__":
    main()
