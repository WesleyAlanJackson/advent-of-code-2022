import os

ABSOLUTE_PATH = os.path.dirname(__file__)
INPUT_PATH = os.path.join(ABSOLUTE_PATH, "input.txt")

def get_num_overlaps_part_1(sections: list):
    num = 0
    for s in sections:
        assignment_1, assignment_2 = s.split(",")
        fr = [int(x) for x in assignment_1.split("-")]
        sr = [int(x) for x in assignment_2.split("-")]

        if (fr[0] <= sr[0] and fr[1] >= sr[1]) or (sr[0] <= fr[0] and sr[1] >= fr[1]):
            num += 1

    return num

def get_num_overlaps_part_2(sections: list):
    num = 0
    for s in sections:
        assignment_1, assignment_2 = s.split(",")
        fr = [int(x) for x in assignment_1.split("-")]
        sr = [int(x) for x in assignment_2.split("-")]

        if sr[1] >= fr[0] >= sr[0] or fr[1] >= sr[0] >= fr[0]:
            num += 1

    return num


def main():

    with open(INPUT_PATH, 'r') as f:
        sections = f.readlines()
        print(get_num_overlaps_part_1([s.strip() for s in sections]))
        
    with open(INPUT_PATH, 'r') as f:
        sections = f.readlines()
        print(get_num_overlaps_part_2([s.strip() for s in sections]))

    
if __name__ == "__main__":
    main()