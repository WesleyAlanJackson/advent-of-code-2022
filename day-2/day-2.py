import os

ABSOLUTE_PATH = os.path.dirname(__file__)
INPUT_PATH = os.path.join(ABSOLUTE_PATH, "input.txt")


def get_strategy_score_part_1(strat: list):
    PLAY_MAP = {"X": {"points": 1, "beats": "Z"}, "Y": {"points": 2, "beats": "X"}, "Z": {"points": 3, "beats": "Y"}}
    EQUIVALENT = {"A": "X", "B": "Y", "C": "Z"}
    score = 0
    for s in strat:
        play = s[1]
        score += PLAY_MAP.get(play).get("points")
        e = EQUIVALENT.get(s[0])
        if play == e:
            score += 3
        elif PLAY_MAP.get(play).get("beats") == e:
            score += 6
    
    return score


def get_strategy_score_part_2(strat: list):
    PLAY_MAP = {"A": {"points": 1, "beats": "C", "loses": "B"}, "B": {"points": 2, "beats": "A", "loses": "C"}, "C": {"points": 3, "beats": "B", "loses": "A"}}
    score = 0
    for s in strat:
        opp_play = s[0]
        result = s[1]
        if result == "X":
            play = PLAY_MAP.get(opp_play).get("beats")
        elif result == "Y":
            play = opp_play
            score += 3
        else:
            play = PLAY_MAP.get(opp_play).get("loses")
            score += 6

        score += PLAY_MAP.get(play).get("points")

    return score


    
def main():

    with open(INPUT_PATH, 'r') as f:
        strategy = f.readlines()
        print(get_strategy_score_part_1([s.strip().replace(" ", "") for s in strategy]))
        print(get_strategy_score_part_2([s.strip().replace(" ", "") for s in strategy]))
    
if __name__ == "__main__":
    main()